rollback;


BEGIN;
CREATE TABLE dar_user ( 
user_id serial PRIMARY KEY,
email VARCHAR UNIQUE NOT NULL,
password VARCHAR NOT NULL
);

CREATE TABLE exams ( 
exam_id serial PRIMARY KEY,
title VARCHAR
);

CREATE TABLE user_exams ( 
user_id INT NOT NULL REFERENCES dar_user (user_id),
email_id INT NOT NULL REFERENCES exams (exam_id),
timestamp_start TIMESTAMP WITH TIME ZONE,
timestamp_end TIMESTAMP WITH TIME ZONE
);


CREATE TABLE problems (
problem_id SERIAL PRIMARY KEY,
question VARCHAR,
exam_id INT NOT NULL REFERENCES exams (exam_id),
image BYTEA
);

CREATE TABLE answers ( 
answer_id SERIAL PRIMARY KEY,
image BYTEA NOT NULL,
correct BOOLEAN NOT NULL,
problem_id INT REFERENCES problems (problem_id)
);

CREATE TABLE user_answers (
  user_id INTEGER REFERENCES dar_user(user_id) NOT NULL,
  stamp TIMESTAMP WITH TIME ZONE NOT NULL,
  answer_id INTEGER REFERENCES answers(answer_id) NOT NULL
);

COMMIT;

--TEST DATA
INSERT INTO exams (title) VALUES ('exam1'), ('exam2'), ('exam3'), ('exam4');
INSERT INTO problems(question,exam_id,image) VALUES ('What is Darin''s favorite fruit?',1,''),('What is the smartest toy dog breed?',1,'');
INSERT INTO answers(image,correct,problem_id) VALUES ('','t',1),('','f',1),('','t',2),('','f',2);
