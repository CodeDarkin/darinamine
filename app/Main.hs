{-# LANGUAGE OverloadedStrings, ScopedTypeVariables, TypeApplications #-}
module Main where
import Web.Scotty as S
import Data.Monoid (mconcat)
import Text.Blaze.Html5 as H hiding (main,map)
import Text.Blaze.Html5.Attributes as A
import Text.Blaze.Html.Renderer.Text
import Database.PostgreSQL.Simple
import Control.Monad.IO.Class
import Control.Monad
import Database.PostgreSQL.Simple.TypeInfo.Static (_daterange)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (fromStrict)
import qualified Data.ByteString.Lazy as BSL
import Data.String (fromString)
import Web.Scotty.Cookie
import qualified Data.Text as T
import Network.HTTP.Types.Status
import Data.List (intercalate)
import Data.Time.Clock

main = scotty 3000 $ do
    get "/demo/:word" $ do
        beam <- S.param "word"
        S.html ( mconcat ["<h1>Scotty, ", beam, " me up!</h1>"] )
    get "/createlogin" createlogin
    post "/createlogin" createlogin2
    get "/exams" exams
    get "/exam/:exam_id" exam
    get "/login" login 
    post "/login" login2
    get "/problem_image/:problem_id" problemImage
    get "/answer_image/https://hackage.haskell.org/package/ptime:answer_id" answerImage
    post "/useranswer" userAnswer
    get "/exam.css" examCss
    get "/result/:exam_id" examResult

connectDB :: IO Connection
connectDB = connectPostgreSQL "dbname=darinamine"

createlogin = S.html $ renderHtml $
    docTypeHtml $ do
        H.head $ do
            H.title "Create Login"
        H.body $ do
            H.form ! A.method "post" $ do 
                H.label "username" 
                input ! name "username"
                H.label "password" 
                input ! name "pw"
                input ! type_ "submit" ! value "create"

createlogin2 = do 
    --take username, pw from the form, insert into db
    --error handling (no username, no pw, duplicate username), show previous form
    username :: String <- S.param "username"
    password :: String <- S.param "pw"
    new_user :: [Only Int] <- liftIO $ do
        db <- connectDB 
        query db "insert into dar_user (email, password) values (?,crypt(?, gen_salt('md5'))) RETURNING user_id" [username, password]
    let userid = T.pack $ show $ fromOnly $ Prelude.head new_user
    setSimpleCookie "user_id" userid
    redirect "/exams"

login = do
    error :: String <- S.rescue (S.param "error") (\_ -> pure "")
    S.html $ renderHtml $
      docTypeHtml $ do
        H.head $ do
            H.title "Login"
        H.body $ do
            H.form ! A.method "post" $ do 
                H.label "username" 
                input ! name "username"
                H.label "password" 
                input ! name "pw"
                input ! type_ "submit" ! value "login"
            H.div $ (toHtml error)

login2 = do 
    --take username, pw, check if there is a match in the db, if yes set cookie and redirect to exams
        --if not in db, stay on login page and return error to user
    username :: String <- S.param "username"
    password :: String <- S.param "pw" 
    match :: [Only Int] <- liftIO $ do 
        db <- connectDB 
        query db "select user_id from dar_user where email = ? and password = crypt(?, password)" [username, password]
    if null match then 
        redirect "/login?error=invalid email and/or password"
    else do
        let userid = T.pack $ show $ fromOnly $ Prelude.head match
        setSimpleCookie "user_id" userid
        redirect "/exams"


exams = do 
    allexams :: [(Int, String)] <- liftIO $ do
        db <- connectDB 
        query_ db "SELECT exam_id, title FROM exams"
    S.html $ renderHtml $
        docTypeHtml $ do 
            H.head $ do 
             H.title "Exams"
            H.body $ do 
             H.ul $
                forM_ allexams $ \(exam_id, title) ->
                    H.li $ H.a ! A.href (H.textValue ("/exam/" <> T.pack (show exam_id))) $ toHtml title

examInfo :: T.Text -> Int -> IO (String, [(Int, String)], [(Int,Bool,Int)], [(UTCTime, Int)])
examInfo userid examid = do
    db <- connectDB
    -- TODO: should user see his previously selected answers when returning to an exam? If so, add to query.
    examtitle :: [Only String] <- query db "SELECT e.title from exams as e where exam_id=?" [examid]
    problems :: [(Int, String)] <- query db "select problem_id, question from problems where exam_id=?" [examid]
    answers :: [(Int, Bool, Int)] <- query db "select answer_id, correct, problem_id from answers where problem_id in (select problem_id from problems where exam_id = ?)" [examid]
    user_answers :: [(UTCTime, Int)] <- query db "select MAX(ua.stamp),ua.answer_id from user_answers as ua join answers as a on ua.answer_id = a.answer_id join problems as p on p.problem_id = a.problem_id join exams as e on e.exam_id = p.exam_id where ua.user_id=? and e.exam_id=? group by ua.answer_id, ua.stamp;" (userid, examid)

    pure (fromOnly (Prelude.head examtitle), 
            problems, 
            answers,
            user_answers)

-- input exam_id
-- 1. load questions and answers from db 2. list questions from exam and possible answers
exam = do
    userid <- getCookie "user_id"
    case userid of 
        Nothing -> raiseStatus forbidden403 "access denied, please log in for access"
        Just userid -> do
            examid :: Int <- S.param "exam_id"
            (examtitle, problems, answers, _) <- liftIO $ examInfo userid examid
                
            S.html $ renderHtml $ 
            --when answer is clicked, send post request to useranswer
                docTypeHtml $ do
                    H.head $ do 
                        H.link ! A.rel "stylesheet" ! A.href "/exam.css"
                        let questionBlock = "var questions = [" <> intercalate "," (map mkProblemInfo problems) <> "];"
                            mkProblemInfo (pid, question) =
                                let currentAnswers = filter (\(_,_,pid')->pid' == pid) answers
                                in
                                "{problem_id:" <> show pid <> 
                                ",question:\"" <> question <> "\"" <>
                                ",selected_answer:null" <>  
                                ",choices:[" <> intercalate "," (map mkAnswerInfo currentAnswers) <> "]}"
                            mkAnswerInfo (aid, _, _) = show aid
                            submitAnswer = "function submitAnswer (e)"
                             <>"{const answerid = e.currentTarget.dataset.aid;"
                             <>"const fd = new FormData();"
                             <>"fd.append('answer_id', answerid);"
                             <>"fetch('/useranswer', {method: 'POST', body:fd})}"
                            contentLoaded = " document.addEventListener('DOMContentLoaded', ()=> { document."
                             <>"querySelectorAll('input[type=radio]').forEach(answer => answer."
                             <>"addEventListener('click', submitAnswer));"
                             <>"document.getElementById('prev-btn').addEventListener('click', (e)=>changeProblem(e, -1));\n"
                             <>"document.getElementById('next-btn').addEventListener('click', (e)=>changeProblem(e, +1));\n"
                             <> "showProblem("<> show firstProblemid <> ") });\n"
                            firstProblemid = case problems of 
                                                (problemid,_):_-> problemid
                                                [] -> -1
                            showProblem = "function showProblem(pid)"
                             <>"{var problemIndex = questions.findIndex(e=>e.problem_id==pid);\n"
                             <>"var problem = questions[problemIndex];\n"
                             <>"document.getElementById('question_image').src=`/problem_image/${problem.problem_id}`;"
                             <>"document.getElementById('question_text').textContent=problem.question;"
                             <>"document.getElementById('choices').innerHTML='';"
                             <>"document.getElementById('problem_id').value = pid;"
                             <>"for(var answer of problem.choices)"
                             <>"document.getElementById('choices').innerHTML +=`"
                             <>"<img src=\"/answer_image/${answer}\" "
                             <> "data-aid=\"${answer}\" "
                             <>"class=\"answer ${answer.selected ? 'selected' : ''}\""
                             <> "/>`;\n"
                             <> "function clickAnswer(event){"
                             <> "let answer_id = event.currentTarget.dataset.aid;\n"
                             <> "problem.selected=answer_id;\n"
                             <> "document.querySelectorAll('.answer').forEach(a=>a.classList.remove('selected'));\n"
                             <> "document.querySelector(`img[data-aid=\"${answer_id}\"]`).classList.add('selected');\n"
                             <> "submitAnswer(event);"
                             <> "}\n"
                             <> "document.querySelectorAll('.choices img').forEach(e=>e.addEventListener('click', clickAnswer));\n"
                             --disable next or previous buttons as appropriate
                             <> "var prev = document.getElementById('prev-btn');\n"
                             <> "var next = document.getElementById('next-btn');\n"
                             <> "prev.removeAttribute('disabled');\n"
                             <> "next.removeAttribute('disabled');\n"
                             <> "if(problemIndex==0) prev.setAttribute('disabled','disabled');\n"
                             <> "if(problemIndex==questions.length-1) next.setAttribute('disabled','disabled');\n"
                             <> "}"
                            changeProblem = "function changeProblem(e, count)\n"
                              <> "{ var current_pid = document.getElementById('problem_id').value;\n"
                              <> "var next_pid = parseInt(current_pid) + count;\n"
                              <> "showProblem(next_pid);\n" -- add bounds checking to pid
                              <> "e.preventDefault();\n"
                              <> "}"
                        

                        -- H.title (toHtml (fromOnly (Prelude.head examtitle)))
                        H.script $ toHtml (contentLoaded <> questionBlock <> submitAnswer <> showProblem <> changeProblem)
                        H.body $ do
                         H.form ! A.method "post" $ do
                            H.input ! A.type_ "hidden" ! A.name "exam_id" ! A.value (fromString (show examid))
                            H.input ! A.type_ "hidden" ! A.name "problem_id" ! A.id "problem_id"
                            H.div ! A.class_ "test-container" ! A.id "test-container" $ do
                                H.div ! A.class_ "question_image-container" $ 
                                  H.img ! A.id "question_image"
                                H.div ! A.id "question_text" $ pure ()
                                H.div ! A.class_ "choices" ! A.id "choices" $ pure ()
                                H.div ! A.class_ "actions" $ do
                                    H.button ! A.class_ "btn" ! A.id "prev-btn" $ toHtml @String "Previous"
                                    H.button ! A.class_ "btn" ! A.id "next-btn" $ toHtml @String "Next"
                                    H.a ! A.href "results" $ do
                                        H.button ! A.class_ "btn" ! A.id "results" $ toHtml @String "View Results"

                           {- H.input ! A.type_ "hidden" ! A.value (fromString (show examid))
                            forM_ problems $ \(pid, question, image) -> do
                                H.p (toHtml question)
                                -- add image
                                H.img ! A.src (fromString ("/problem_image/" <> show pid))
                                let currentAnswers = filter (\(_,_,_,pid')->pid' == pid) answers
                                forM_ currentAnswers $ \(aid, _, image, _) -> do
                                    H.input ! A.type_ "radio" ! A.name (fromString (show pid)) ! A.value (fromString (show aid))
                                    H.img ! A.src (fromString ("/answer_image/" <> show aid))
                            --H.input ! A.type_ "submit" ! A.value "Submit Answers"
-}
{-
	<div class="container">
		<h1>Multiple Choice Test</h1>
        <div class="alert" id="alert"></div>

        <div class="test-container" id="test-container">
            <div class="question" id="question"></div>
            <div class="choices" id="choices"></div>
            <div class="actions">
                <button class="btn" id="prev-btn" onclick="prevQuestion()" disabled>Prev</button>
                <button class="btn" id="next-btn
                <button class="btn" id="prev-btn" onclick="prevQuestion()" disabled>Prev</button>
                <button class="btn" id="next-btn" onclick="nextQuestion()">Next</button>
            </div>
        </div>
    
        <div class="result-container" id="result-container">
            <div class="result" id="result"></div>
            <div class="result-info" id="result-info"></div>
            <div class="result-actions">
                <button class="btn" onclick="restartTest()">Restart Test</button>
            </div>
        </div>
    </div>
-}                            

-- returns the image data for one problem
problemImage = do
    pid :: Int <- S.param "problem_id"
    images :: [Only ByteString] <- liftIO $ do
        db <- connectDB 
        query db "SELECT image FROM problems WHERE problem_id = ?" [pid]
    let image = fromStrict (fromOnly (Prelude.head images))
    S.setHeader "Content-type" "image/jpeg"
    S.raw image

answerImage = do
    aid :: Int <- S.param "answer_id"
    images :: [Only ByteString] <- liftIO $ do
        db <- connectDB 
        query db "SELECT image FROM answers WHERE answer_id = ?" [aid]
    let image = fromStrict (fromOnly (Prelude.head images))
    S.setHeader "Content-type" "image/jpeg"
    S.raw image

-- takes the user's answer and saves it to the db
userAnswer = do
    userid <- getCookie "user_id"
    case userid of 
        Nothing -> raiseStatus forbidden403 "access denied, please log in for access"
        Just userid -> do
            answer_id :: T.Text <- S.param "answer_id" 
            liftIO $ do
                db <- connectDB 
                execute db "INSERT INTO user_answers (user_id, stamp, answer_id) VALUES (?, now() ,?)" [userid, answer_id]
                pure ()

-- show the result for a completed exam including correct and incorrect answers and user-selected choice
examResult = do  
    userid <- getCookie "user_id"
    case userid of 
        Nothing -> raiseStatus forbidden403 "access denied, please log in for access"
        Just userid -> do
            examid :: Int <- S.param "exam_id"
            (examtitle, problems, answers, user_answers) <- liftIO $ examInfo userid examid
                
            S.html $ renderHtml $ 
            --when answer is clicked, send post request to useranswer
                docTypeHtml $ do
                    H.head $ do 
                        H.link ! A.rel "stylesheet" ! A.href "/exam.css"
                        H.p $ toHtml (show user_answers)

examCss = do
  S.setHeader "Content-type" "text/css"
  let css = 
        "body {" <>
        "font-family: Arial, sans-serif;" <>
        "background-color: #F6F9FC;" <>
        "margin: 0;" <>
        "padding: 0;}" <>
        ".container {" <>
        "max-width: 800px;" <>
        "margin: 0 auto;" <>
        "padding: 20px;}" <>
        "h1 {" <>
        "text-align: center;" <>
        "margin-top: 50px;" <>
        "margin-bottom: 50px;" <>
        "color: #333;}" <>
        ".alert {" <>
        "background-color: #FDDCDC;" <>
        "color: #721C24;" <>
        "padding: 10px;" <>
        "margin-bottom: 20px;" <>
        "border-radius: 5px;" <>
        "display: none;}" <>
        ".test-container {" <>
        "max-width: 800px;" <>
        "margin: 0 auto;" <>
        "background-color: #FFF;" <>
        "border-radius: 5px;" <>
        "padding: 20px;" <>
        "box-shadow: 0px 0px 10px #EEE;}" <>
        ".question {" <>
        "font-size: 18px;" <>
        "font-weight: bold;" <>
        "margin-bottom: 10px;" <>
        "color: #333;}" <>
        ".choices img {" <>
        "display: inline-block;" <>
        "margin-right: 10px;" <>
        "max-width: 100%;" <>
        "height: auto;" <>
        "margin-bottom: 10px;" <>
        "border-radius: 5px;" <>
        "cursor: pointer;}" <>
        ".choices img.selected {" <>
        "border: 2px solid #007BFF;" <>
        "}" <>
        ".actions {" <>
        "margin-top: 20px;" <>
        "text-align: center;}" <>
        ".btn {" <>
        "background-color: #007BFF;" <>
        "color: #FFF;" <>
        "border: none;" <>
        "border-radius: 5px;" <>
        "padding: 10px 20px;" <>
        "cursor: pointer;" <>
        "transition: all 0.2s;" <>
        "margin-right: 10px;}" <>
        ".btn:hover {" <>
        "background-color: #0069D9;}" <>
        ".btn:disabled {" <>
        "opacity: 0.5;" <>
        "cursor: not-allowed;}" <>
        ".result-container {" <>
        "background-color: #FFF;" <>
        "border-radius: 5px;" <>
        "padding: 20px;" <>
        "box-shadow: 0px 0px 10px #EEE;" <>
        "display: none;}" <>
        ".result {" <>
        "font-size: 36px;" <>
        "font-weight: bold;" <>
        "margin-bottom: 10px;" <>
        "color: #333;" <>
        "text-align: center;}" <>
        ".result-info {" <>
        "font-size: 18px;" <>
        "margin-bottom: 20px;" <>
        "color: #333;" <>
        "text-align: center;}" <>
        ".result-actions {" <>
        "margin-top: 20px;" <>
        "text-align: center;}" :: BSL.ByteString
  S.raw css

{-
TODO:
* fix user-id cookie security hole with encrypted cookie
* show completion checkmark for users on the exams page
* css styling
* answer submission check
* 14-day timer
    ajax to submit each indiv answer immediately to user_answers table
    14 day streak invalidator, set to 24 hrs
    mouse activity tracker for 10 min, and/or popup asking if user is still there
-}