from PIL import Image

def paper_fold_slice(image):
    #the paper fold tests have answers that do not abut each other, so we can use the whitespace between them to discern them
    pixel_threshold = 225
    (question_img, answers_img) = partition_question_answer_images(image, pixel_threshold)
    question_img.save('qimg.png')
    answers_img.save('answers.png')
    px = answers_img.load()
    (width, height) = answers_img.size
    coliter = iter(range(0, width))
    whiteregion = []
    slicepositions = [0]
    for col in coliter:
        whitepixelcount = 0
        for row in range(0,height):
            if px[col, row] >= pixel_threshold: #white is 255, black is no color- 0
                whitepixelcount += 1
        #print(col,whitepixelcount, .95 * height, whiteregion)
        if whitepixelcount > .95 * height:
            #white column- find 3 large white consecutive regions
            if len(whiteregion) == 0 or (len(whiteregion) > 0 and col - whiteregion[-1] == 1):
                whiteregion.append(col)
            else: #create a new region
                if whiteregion[-1] - whiteregion[0] > 10:
                    slicepositions.append(whiteregion[0])
                    slicepositions.append(whiteregion[-1])
                whiteregion = []
        else: #hit black region, so flush the white region
            if len(whiteregion) > 0 and whiteregion[-1] - whiteregion[0] > 10:
                slicepositions.append(whiteregion[0])
                slicepositions.append(whiteregion[-1])
                whiteregion = []
    slicepositions.append(width)
    print(slicepositions)
    assert len(slicepositions) == 8, "Wrong number of white regions found for paper fold test."
    all_images = [question_img]
    for i in range(0,4):
        left_crop = slicepositions[i*2]
        right_crop = slicepositions[i*2+1]
        #l t r b
        all_images.append(answers_img.crop((left_crop,1,right_crop,height-1)))
    return all_images

def slice(image):
    if image.size[0] > 400: #paper fold question!
        return paper_fold_slice(image)
    slicepositions = []
    pixel_threshold = 225
    #find question image because it may be a different size from the answer images
    (question_img, answers_img) = partition_question_answer_images(image, pixel_threshold)
    question_img.save('qimg.png')
    answers_img.save('answers.png')
    px = answers_img.load()
    (width, height) = answers_img.size
    coliter = iter(range(0, width))
    for col in coliter:
        blackpxcount = 0
        values = []
        for row in range(0, height):
            values.append(px[col,row])
            if px[col, row] < pixel_threshold: #white is 255, black is no color- 0
                blackpxcount += 1
        pxcount_threshold = .95 * height
        #print(col, blackpxcount, pxcount_threshold)
        if blackpxcount == 0:
            continue
        elif blackpxcount > pxcount_threshold:
            if len(slicepositions) % 2 != 0 and col - slicepositions[-1] > 1:
                #if we have an odd number of slicepositions and the previous sliceposition is more than one pixel away, that means that our heuristic failed to find the second black line, so just assume it should have been picked up
                slicepositions.append(slicepositions[-1]+1)
            if len(slicepositions) > 0 and len(slicepositions) % 2 == 0 and col - slicepositions[-1] < 40:
                # if we have an even number of slicepositions and the previous sliceposition is just one pixel away, we should ignore it
                pass
            else: 
                slicepositions.append(col)
    if len(slicepositions) == 9:
        # if we have nine points, then the last slice didn't get two points, so just add one
        slicepositions.append(slicepositions[-1]+1)
    question_img.save('out_question.png')        
    answers_img.save('out_answers.png') # save original image + dots at black pixel columns for debugging
    print(slicepositions)
    assert len(slicepositions) == 10, "Incorrect number of slice positions."
    leftcrop_x = slicepositions[0]
    rightcrop_x = slicepositions[3]
    topcrop, bottomcrop = top_bottom_slice(answers_img)
    #questionimage.save('out0.png')
    all_images=[question_img]
    for imgcount in range(0,4):
        leftcrop = slicepositions[imgcount*2]
        rightcrop = slicepositions[imgcount*2+3]
        cropped = answers_img.crop((leftcrop, topcrop, rightcrop+1, bottomcrop))
        all_images.append(cropped)
        #cropped.save(f'out{imgcount}.png')
    return all_images

def partition_question_answer_images(img, pixel_threshold):
    #look for first completely white lines and slice it out, return two images on the left and right of the whitespace
    px = img.load()
    width = img.size[0]
    height = img.size[1]
    coliter = iter(range(5, width)) # jump 5 pixels in so that we don't consider the image border
    vertical_white_columns = []
    for col in coliter:
        whitepixelcount = 0
        blackpixelcount = 0
        for row in range(0, height):
            if px[col, row] > pixel_threshold: #if white pixel
                whitepixelcount += 1
            else:
                blackpixelcount += 1
        if blackpixelcount > 1 and len(vertical_white_columns) > 0:
            break
        if whitepixelcount > height - 3:
            vertical_white_columns.append(col)
    #l t r b
    #print(vertical_white_columns)
    question_img = img.crop((0,0,vertical_white_columns[0], height))
    question_crop = top_bottom_slice(question_img)
    question_img = question_img.crop((0, question_crop[0], question_img.size[0], question_crop[1]))

    answers_img = img.crop((vertical_white_columns[-1], 0, width, height))
    answers_crop = top_bottom_slice(answers_img)
    answers_img = answers_img.crop((0, answers_crop[0], answers_img.size[0], answers_crop[1]))
    return (question_img,
            answers_img)

def top_bottom_slice(image):
    """Scan image vertically to find the top and bottom white areas we can crop out."""
    px = image.load()
    rowiter = iter(range(0, image.size[1]))
    pixel_threshold = 225
    firstblackrow = 0
    lastblackrow = image.size[1]
    for row in rowiter:
        blackpxcount = 0
        for col in range(0, image.size[0]):
            if px[col, row] < pixel_threshold: #white is 255, black is no color- 0
                blackpxcount += 1
        if blackpxcount > .9 * image.size[0]:
            firstblackrow = row
            break

    for row in range(image.size[1]-1, 0, -1):
        blackpxcount = 0
        for col in range(0, image.size[0]):
            if px[col, row] < pixel_threshold: #white is 255, black is no color- 0
                blackpxcount += 1
        if blackpxcount > .9 * image.size[0]:
            lastblackrow = row
            break
    return (firstblackrow -1, lastblackrow+1)
    

if __name__ == "__main__":
    for (i,img) in enumerate(slice(Image.open("in.png").convert('L'))):
        img.save(f'out{i}.png')
