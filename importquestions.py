'''Get the question
Get the image and slice into 5 images accordingly
Get the answer
(optional if there) Get the Explanation image
import exam title, question, images, answer, explanation image into the database

configurations: image cropping, url,  '''

from PIL import Image
from bs4 import BeautifulSoup
import psycopg2, requests
from io import BytesIO
from sqlalchemy import text, create_engine
import sys

sys.path.append(".")

from slice import slice

conf = [
    
{'url':'https://www.indiabix.com/non-verbal-reasoning/image-analysis/',
'coordinates': [(0,0,93,70), (154,21,202,72), (199,20,248,71), (243,19,292,71),(287,19, 337,71)]},

{'url': 'https://www.indiabix.com/non-verbal-reasoning/image-analysis/033001',
'coordinates': [(0,0, 47, 49), (131,0,179,49), (176,0,223,49), (221,0, 267,49), (265,0,313,49)]},

{'url': 'https://www.indiabix.com/non-verbal-reasoning/image-analysis/034001',
'coordinates': [(0,0, 47, 49), (131,0,181,49), (178,0,225,49), (222,0, 269,49), (266,0,314,49)]},

{'url':'https://www.indiabix.com/non-verbal-reasoning/paper-folding/',
'coordinates': [(0,0, 58,59), (106,0,165,60), (194,0,253,59), (281,0, 342,59), (370,0,432,61)]},

{'url':'https://www.indiabix.com/non-verbal-reasoning/paper-folding/020001',
'coordinates': [(0,0, 58,59), (106,0,165,60), (194,0,256,61), (281,0, 344,59), (370,0,431,61)]},

{'url':'https://www.indiabix.com/non-verbal-reasoning/embedded-images/',
'coordinates': [(0,0, 58,60), (82,0,141,60), (140,0,198,60), (193,0, 254,60), (250,0,311,61)]},

{'url':'https://www.indiabix.com/non-verbal-reasoning/embedded-images/014001',
'coordinates': [(0,0, 59,61), (84,0,142,61), (138,0,198,61), (195,0, 254,61), (250,0,311,61)]}]


#open url with beautifulsoup, get exam title from h3 class "overview" child text,
#get question from bix-td-qtxt, second p tag (iterate for each question), get image from third p tag, img, src
#get answer from bix-div-answer, p tag, second span
#if there is an image in bix-div-answer, get that too for the explanation image


def import_exam(db, url, coordinates):
    questions = []
    next_url = url
    i=0
    while next_url:
        print(next_url)
        response = requests.get(next_url).text
        soup = BeautifulSoup(response, 'html.parser')
        exam_title = soup.find("h3", {"class":"overview"}).text
        for question in soup.select("td.bix-td-qtxt"):
            qtxt = question.select_one('p:nth-of-type(1)')
            qimg = question.select_one('img')
            print(question)
            questions.append({'question':qtxt.text, 'qimg': 'https://www.indiabix.com' + qimg['src']})
        for answer in soup.select("div.bix-div-answer "):
            anstxt = answer.select_one('p:first-of-type > span:nth-of-type(2)')
            ansimg = answer.select_one('div.bix-ans-description > p > img')
            questions[i]["answer"] = anstxt.text
            questions[i]["answerimg"] = 'https://www.indiabix.com' + ansimg['src'] if ansimg else None
            i+=1
        next = soup.select_one("p.mx-pager > a:last-of-type")
        if 'Next' not in next.text:
            next_url = None
        else:
            next_url = 'https://www.indiabix.com' + next['href']
    print(next_url)
    
    print(exam_title)
    print(questions)
    for question in questions:
        response = requests.get(question['qimg'])
        img = Image.open(BytesIO(response.content)).convert('L')
        print(question['qimg'])
        images = slice(img)
        def imgtopng(img):
            h = BytesIO()
            #bbox = img.getbbox()
            #img = img.crop(bbox)
            img.save(h, format='png')
            return h.getvalue()
        images = list(map(imgtopng,images))
        i=0
        question['referenceimg'] = images[0]
        question['optionimgs'] = images[1:]
        '''for image in images:
            image.save(f'/tmp/{i}.png')
            i+=1'''
    importdb(db, questions,exam_title)
            
def importdb(db, questions, examtitle):
    #connect to postgresql, sql insert for problems and answers, exams.title for examtitle, one transaction
    '''
    
    INSERT INTO problems(question,exam_id,image) VALUES (?,?,?,?);
    INSERT INTO answers(image,correct,problem_id) VALUES (?,?,?);
    '''
    exam_id = db.execute(text('INSERT INTO exams(title) VALUES (:title) RETURNING exam_id;'), {'title':examtitle}).scalar()
    for question in questions:
        problem_id = db.execute(text('INSERT INTO problems(question,exam_id,image) VALUES (:question,:exam_id,:image) RETURNING problem_id;'), 
          {'question':question['question'], 'exam_id':exam_id, 'image':question['referenceimg']}).scalar()
        correct_index = ord(question['answer']) - ord('A')
        #convert correct answer "B" into array index
        c = 0
        for answer in question['optionimgs']:
            db.execute(text('INSERT INTO answers(image,correct,problem_id) VALUES (:image,:correct,:problem_id)'), 
              {'image':answer,'correct':c==correct_index,'problem_id':problem_id})
            c+=1

#detect edge of image and not slice beyond it, line 113. change box before cropping
'''def slice(img_url, coordinates):
    #download image from url and slice with pillow
    response = requests.get(img_url)
    img = Image.open(BytesIO(response.content))
    all_images=[]
    for box in coordinates:
        all_images.append(img.crop(box))
    return all_images'''

    
#delete all exam data in the database
def sweep_db(db):
    db.execute(text('DELETE FROM user_answers'))
    db.execute(text('DELETE FROM answers'))
    db.execute(text('DELETE FROM problems'))
    db.execute(text('DELETE FROM exams'))

    
engine = create_engine('postgresql://localhost/darinamine')
db = engine.connect()
sweep_db(db)
for examinfo in conf:
    import_exam(db, examinfo['url'], examinfo['coordinates'])
#import_exam(db, conf[6]['url'], conf[6]['coordinates'])
